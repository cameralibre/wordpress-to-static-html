import os
from webptools import grant_permission
from webptools import cwebp
# this will grant 755 permission to webp executables
grant_permission()

files = os.listdir()
images = [file for file in files if file.endswith(('jpg', 'png', 'tif'))]

def convert_image(image_path, quality):
  image_name = image_path.split('.')[0]
  print(cwebp(input_image=f"{image_path}", output_image=f"{image_name}.webp", option=f"-q {quality}", logging="-v"))

for image in images:
  convert_image(image, 70)