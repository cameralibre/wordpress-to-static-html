# Wordpress to static HTML

When you're still paying a monthly fee for hosted Wordpress, years after your last update to a site, it _maaaay_ be time to 'archive it' and convert it to static HTML. That way it can be hosted on Gitlab pages or similar, for free, and in a much more lightweight form.

I still don't really know how to write Python so this is kinda kludged together, but it works for my website (http://cameralibre.cc) so maybe it can be adapted to become useful for other Wordpress sites.

Characteristics of my site which may be relevant:

- uses the [Divi](https://www.elegantthemes.com/gallery/divi/) theme/builder
- doesn't have comments or a contact form
- doesn't need to have an RSS feed anymore (it's not going to be updated with new posts)
- has basically the same layout for 99% of the site

## REPO CONTENTS

- the main file, `wordpress_to_static_html.py` will scrape a site for find all of its pages (or at least, all the local pages which are linked to from other pages), then download a modify an `.html` file for each page.
- `convert_images_to_webp` will convert `.jpg`, `.png` (and `.tif` — yes, I had a `.tif` on my website 🙄) into the modern and lightweight WebP image format. It will convert all the images in the directory where you call the script.
