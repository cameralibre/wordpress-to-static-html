import os
import sys
import re
import requests
import pickle
from typing import Optional, List
from pprint import pprint
from bs4 import BeautifulSoup, Comment  # type: ignore
from bs4.element import NavigableString, Tag, PageElement, ResultSet


def strip_protocol(url: str):
    return re.sub(r"(^https?:\/\/)(www.)?", "", url)


def create_site_directory(domain: str):
    directory_path = os.path.join(os.getcwd(), f'{domain.replace(".","-")}_pages')
    if not os.path.exists(directory_path):
        os.mkdir(directory_path)
    return directory_path


def write_file(file_path: str, content: str):
    file = open(file_path, "w")
    file.write(content)
    file.close()


def write_extract_file(directory: str, selector: str, content: str):
    file_name = selector.lstrip("#.") + ".html"
    print(file_name)
    write_file(f"{directory}/{file_name}", content)


def extract_global_elements(domain: str, site_directory: str):
    """extract the repeating elements that appear on all pages.

    The original head, header, footer and style are saved to their own files,
    where they can be copied from, manually edited or cleaned up. The modified
    components can be placed in {site_directory}/components so they can be
    re-inserted into cleaned-up pages. (This happens in add_custom_components,
    the next time the script is run).
    """

    extracts_directory = f"{site_directory}/extracts"
    if os.path.exists(extracts_directory):
        return

    os.mkdir(extracts_directory)
    response: requests.models.Response = requests.get(f"http://{domain}")
    soup = BeautifulSoup(response.content, "html.parser")

    global_singleton_elements = ["head", "header", "#top-header", "footer"]
    global_multiple_elements = ["style", "script"]

    print("writing component files:")
    for selector in global_singleton_elements:
        el = soup.select_one(selector)
        if el:
            write_extract_file(extracts_directory, selector, el.prettify())

    for selector in global_multiple_elements:
        els = soup.find_all(selector)
        el_strings = map(
            lambda el: el.prettify(),
            els,
        )
        write_extract_file(extracts_directory, selector, "\n".join(el_strings))


def link_is_local(domain: str, link: str) -> bool:
    if len(link) == 0:
        return False
    if link.startswith("#"):
        return False
    if "." not in link:
        return True
    elif link.count(".") == 1 and link.endswith(".html"):
        return True
    else:
        local_prefixes = [
            f"http://www.{domain}",
            f"https://www.{domain}",
            domain,
            f"www.{domain}",
            f"http://{domain}",
            f"https://{domain}",
            "/",
            ".",
        ]
        return link.startswith(tuple(local_prefixes))


def remove_anchor_links(link: str):
    if not "#" in link:
        return link
    url_parts = link.split("/")
    return "/".join(url_parts[:-1])


def trim_trailing_slash(str: str) -> str:
    if len(str) > 0 and str[-1] == "/":
        return str[:-1]
    else:
        return str


def make_absolute(domain: str, url: str, link: str) -> str:
    if link.startswith("/"):
        return "http://" + domain + link
    elif link.startswith("./"):
        return "http://" + domain + link[1:]
    elif "." not in link or link.count(".") == 1 and link.endswith(".html"):
        return "http://" + domain + "/" + link
    elif link.startswith("../"):
        url_parts = (
            trim_trailing_slash(url)
            .replace("http://", "")
            .replace("https://", "")
            .split("/")
        )
        link_parts = link.split("/")
        depth = len(list(filter(lambda part: part == "..", link_parts)))
        new_root = url_parts[: -abs(depth)]
        new_end = filter(lambda part: part != "..", link_parts)
        return "/".join([*new_root, *new_end])
    else:
        return link


def normalise(domain, url, link: str) -> str:
    stripped = link.strip()
    no_www = stripped.replace("www.", "")
    http_only = no_www.replace("https://", "http://")
    no_anchors = remove_anchor_links(http_only)
    no_trailing_slash = trim_trailing_slash(no_anchors)
    absolute_link = make_absolute(domain, url, no_trailing_slash)
    return absolute_link


def is_probably_html(link: str) -> bool:
    # TODO: This should be checking that it is html, rather than checking that it isn't various other things
    extensions = [
        ".jpeg",
        ".jpg",
        ".gif",
        ".png",
        ".pdf",
        ".svg",
        ".mov",
        ".mp4",
        ".mp3",
        ".php",
    ]
    return False if link.lower().endswith(tuple(extensions)) else True


def make_directory(path: str):
    if not os.path.exists(path):
        os.mkdir(path)


def scrape_links(
    domain_name: str, prev_known_links: List[str] = None, site_url: Optional[str] = None
):
    if prev_known_links is None:
        prev_known_links = []
    if site_url is None:
        site_url = "http://" + domain_name

    response: requests.models.Response = requests.get(site_url)
    soup = BeautifulSoup(response.content, "html.parser")
    anchors = soup.findAll("a")
    if len(anchors) == 0:
        print(
            f"no links on this page. Returning existing known links ({len(prev_known_links)})"
        )
        return tuple(prev_known_links)
    else:
        links: List[str] = list(map(lambda el: el["href"], anchors))
        all_locals: List[str] = list(
            filter(lambda link: link_is_local(domain_name, link), links)
        )
        normalised_locals: map[str] = map(
            lambda link: normalise(domain_name, site_url, link), all_locals
        )
        probably_html_only: filter[str] = filter(is_probably_html, normalised_locals)
        unique_local_links: List[str] = list(set(probably_html_only))
        now_known_links: List[str] = list(set([*prev_known_links, *unique_local_links]))
        new_links: List[str] = list(filter(lambda link: link not in prev_known_links, now_known_links))  # type: ignore

        print("\n\nCURRENT PAGE: " + site_url)
        print(len(new_links), "new local links:")
        pprint(new_links, indent=4)
        print("Known links:", len(now_known_links))
        print("External_links (not included):")
        pprint(list(filter(lambda link: link not in all_locals, links)), indent=4)

        if len(new_links) >= 1:
            all_links = set(now_known_links)
            for link in new_links:
                all_links.update(scrape_links(domain_name, list(all_links), link))

            print("All links from " + site_url + " scraped:")
            print("total links:", len(all_links))

            return tuple(all_links)
        else:
            print(
                f"no new links! returning existing known links ({len(prev_known_links)})"
            )

            return tuple(prev_known_links)


def write_pickle(filepath: str, data):
    file = open(filepath, "wb")
    pickle.dump(data, file)
    file.close()


def get_data_from_file(filepath: str):
    file = open(filepath, "rb")
    data = pickle.load(file)
    file.close()
    return data


def unwrap_div(soup, id: str):
    if soup.find("div", {"id": f"{id}"}):
        soup.find("div", {"id": f"{id}"}).unwrap()
    return soup


def make_responsive(el, new_width=""):
    if not el.has_attr("width") or not el.has_attr("height"):
        return
    width = el["width"]
    height = el["height"]
    width_rule = f"width: {new_width};" if new_width else ""
    el["style"] = (
        f"aspect-ratio: {width} / {height}; max-height: {height}px;{width_rule}"
    )
    del el["width"]
    del el["height"]


def make_relative_webp_image(domain: str, img: Tag):
    src = img["src"]
    if domain in src or src.startswith("./"):
        del img["srcset"]
        del img["sizes"]
        relative_src = (
            strip_protocol(src)
            .replace(domain, ".")
            .replace("wp-content/uploads", "images")
        )
        relative_webp = re.sub(r"(.jpg)|(.png)|(.tif)", ".webp", relative_src)
        # cameralibre.cc had some weird filename issue with some images, this may not be relevant for other sites.
        new_src = re.sub(r"-[0-9]*x[0-9]*.webp", ".webp", relative_webp)
        img["src"] = new_src
    return img


def is_empty(el):
    children = list(filter(lambda child: child != "\n" and child != " ", el.contents))
    return len(children) == 0


def is_not_whitespace(text: str):
    return text != "\n" and text != " "


def has_one_child(el: Tag):
    children: List[PageElement] = list(filter(is_not_whitespace, el.contents))
    if len(children) == 1 and type(children[0]) is not NavigableString:
        return True
    return False


def unwrap_if_has_one_child(el: Tag):
    if has_one_child(el):
        child: PageElement = next(filter(is_not_whitespace, el.contents))
        child["class"] = (
            el.get("class")
            if child.get("class") is None
            else [*child.get("class"), el.get("class")]
        )
        el.unwrap()


def delete_classes(delete_list: List[str], soup: BeautifulSoup):
    selector_string = "." + ", .".join(delete_list)
    for el in soup.select(selector_string):
        classes_to_keep = list(
            filter(
                lambda classname: classname not in delete_list,
                el.get("class"),
            )
        )
        if len(classes_to_keep) == 0:
            del el["class"]
        else:
            el["class"] = classes_to_keep


def create_sidebar_link_list(soup: BeautifulSoup, links: ResultSet, id: str):
    heading = soup.new_tag("h2")
    heading["class"] = "sidebar__heading"
    to_title_text = (
        lambda id: id.rstrip("0123456789_").replace("_", " ").replace("-", " ").title()
    )
    heading.append(to_title_text(id))
    link_list = soup.new_tag("ul")
    link_list["class"] = "sidebar__list"

    for a in links:
        new_list_item = soup.new_tag("li")
        new_list_item["class"] = "sidebar__item"
        a["class"] = "sidebar__link"
        new_list_item.append(a)
        link_list.append(new_list_item)

    return [heading, link_list]


def format_sidebar(soup, sidebar):
    new_sidebar = soup.new_tag("nav")
    new_sidebar["class"] = "sidebar"
    section_ids = ["archives-2", "categories-2", "recent-posts-2", "recent-comments-2"]
    for id in section_ids:
        section = sidebar.select_one("#" + id)
        if not section:
            print(f"{id} not found")
        links = section.find_all("a")
        if len(links) == 0:
            print(f"{id} has no links")

        [section_heading, section_link_list] = create_sidebar_link_list(soup, links, id)
        new_sidebar.append(section_heading)
        new_sidebar.append(section_link_list)

    return new_sidebar


def clean_html(site_directory: str, domain: str, url: str):
    response: requests.models.Response = requests.get(url)
    soup = BeautifulSoup(response.content, "html.parser")

    if soup.find("rss"):
        print("rss file, skipping")
        return "rss"

    # remove repeating and non-structural/non-content elements
    for comment in soup(text=lambda text: isinstance(text, Comment)):
        comment.extract()
    for tag in soup.find_all(["script", "style", "link", "meta", "header", "footer"]):
        tag.decompose()
    body = soup.find("body")
    if body and body.has_attr("class"):
        del body["class"]
    if soup.find("div", {"id": "top-header"}):
        soup.find("div", {"id": "top-header"}).decompose()

    # make links relative
    if soup.find("a"):
        for a in soup.find_all("a"):
            href = a["href"]
            if domain in href:
                new_href = (
                    href.replace(f"https://www.{domain}", ".")
                    .replace(f"http://www.{domain}", ".")
                    .replace(f"www.{domain}", ".")
                    .replace(f"{domain}", ".")
                    .replace("wp-content/uploads", "images")
                )
                a["href"] = new_href

    # make image links relative, and point to .webp files
    if soup.find("img"):
        for img in soup.find_all("img"):
            make_relative_webp_image(domain, img)

    # remove useless or obstructing wrappers & elements
    for el in soup.find_all(["div", "p", "h1", "h2", "h3", "h4", "h5", "h6"]):
        if is_empty(el):
            el.decompose()
    for hr in soup.select(".et_pb_space"):
        hr.decompose()
    for div in soup.select(".entry-content, .et_pb_section"):
        div.unwrap()
    for div in soup.select(".container"):
        div.unwrap()
    for span in soup.select('span[style*="font-weight: 400;"]'):
        span.unwrap()
    for id in [
        "page-container",
        "et-main-area",
        "main-content",
        "content-area",
        "left-area",
        "et_post_meta_wrapper",
    ]:
        soup = unwrap_div(soup, id)
    # fix some layout and markup issues with image elements
    for wp_figure in soup.select(".wp-caption"):
        caption = wp_figure.select_one(".wp-caption-text")
        if caption:
            caption.wrap(soup.new_tag("figcaption"))
            caption.unwrap()
        img = wp_figure.select_one("img")
        if img.has_attr("aria-describedby"):
            del img["aria-describedby"]
            make_responsive(img)

        wp_figure.wrap(soup.new_tag("figure"))
        wp_figure.unwrap()

    for pb_blurb in soup.select(".et_pb_blurb"):
        for selector in [".et_pb_blurb_content", ".et_pb_main_blurb_image"]:
            if pb_blurb.select_one(selector):
                pb_blurb.select_one(selector).unwrap()
        caption = pb_blurb.select_one("h6")
        if caption:
            caption.wrap(soup.new_tag("figcaption"))
            caption.unwrap()
        img = pb_blurb.select_one("img")
        if img:
            if img.has_attr("aria-describedby"):
                del img["aria-describedby"]
                make_responsive(img)
                make_relative_webp_image(domain, img)
        else:
            print("no img", img, pb_blurb)

        pb_blurb.wrap(soup.new_tag("figure"))
        pb_blurb.unwrap()

    for gallery in soup.select(".gallery"):
        for item in gallery.select("dl.gallery-item"):
            brs = item.select("br")
            if brs:
                print("brs:", len(brs))
                for br in brs:
                    br.decompose()

            dt = item.select_one("dt")
            if dt:
                dt.unwrap()

            caption = item.select_one(".wp-caption-text")
            if caption:
                caption.wrap(soup.new_tag("figcaption"))
                caption.unwrap()

            item.wrap(soup.new_tag("figure"))
            item.unwrap()

    if soup.select(".et_pb_slider"):
        script = soup.new_tag("script")
        script["src"] = "https://unpkg.com/a11y-slider@latest/dist/a11y-slider.js"
        soup.find("body").append(script)

        for slider in soup.select(".et_pb_gallery.et_pb_slider"):
            slider.select_one(".et_pb_gallery_items").unwrap()
            for gallery_item in slider.select(".et_pb_gallery_item"):
                if gallery_item.select_one(".et_pb_gallery_image"):
                    gallery_item.select_one(".et_pb_gallery_image").unwrap()
                if gallery_item.find("a"):
                    gallery_item.find("a").unwrap()
                if gallery_item.find("span"):
                    gallery_item.find("span").decompose()
                gallery_item.wrap(soup.new_tag("li"))
                gallery_item.unwrap()

            slider_list = soup.new_tag("ul")
            slider_list["class"] = "slider"
            slider.wrap(slider_list)
            slider.unwrap()

        for slider in soup.select(".et_pb_slider"):
            ul = soup.new_tag("ul")
            ul["class"] = "slider"
            slider.wrap(ul)

            if slider.select_one(".et_pb_slides"):
                slider.select_one(".et_pb_slides").unwrap()
            for slide in slider.select(".et_pb_slide"):
                non_image_elements = [
                    ".et_pb_slide_content",
                    ".et_pb_slide_description",
                    ".et_pb_container.clearfix",
                ]
                for selector in non_image_elements:
                    if slide.select_one(selector):
                        el = slide.select_one(selector)
                        if el.string is None:
                            el.decompose()
                li = soup.new_tag("li")
                if slide.get("style"):
                    style = slide.get("style")
                    img_url = re.search(r"(?<=\()(.*?)(?=\))", style).group()
                    if img_url:
                        img = soup.new_tag("img")
                        img["src"] = img_url
                        img["alt"] = ""
                        webp_img = make_relative_webp_image(domain, img)
                        li.append(webp_img)
                        slide.append(li)
                        slide.unwrap()
                    else:
                        print(slide)
                else:
                    slide.wrap(li)

            slider.unwrap()

    video_wrappers = soup.select(".et_post_video")
    for video in video_wrappers:
        iframe = video.select_one("iframe")
        make_responsive(iframe, "100%")
    for image_el in soup.select("img"):
        make_responsive(image_el)
    for p in soup.find_all("p"):
        if p.find("img") and has_one_child(p):
            if has_one_child(p):
                p.unwrap()

    if soup.select(".et_pb_text"):
        for text_wrapper in soup.select(".et_pb_text"):
            text_wrapper.unwrap()
    if soup.select(".figure"):
        for figure_div in soup.select(".figure"):
            figure_div.unwrap()

    for row in soup.select(".et_pb_row"):
        if row.select(".et_pb_column_1_4"):
            row["class"] = "grid grid--col-4"
        if row.select(".et_pb_column_1_3"):
            row["class"] = "grid grid--col-3"
        if row.select(".et_pb_column_1_2"):
            row["class"] = "grid grid--col-2"
        if row.select(".et_pb_column_3_3, .et_pb_column_4_4"):
            row["class"] = "col"
            for column in row.select(".et_pb_column_3_3, .et_pb_column_4_4"):
                column.unwrap()
        if is_empty(row):
            row.decompose()
    for column in soup.select(".et_pb_column_1_3, .et_pb_column_1_4"):
        column["class"] = "col col--span-1"
        unwrap_if_has_one_child(column)
    for column in soup.select(".et_pb_column_2_3, .et_pb_column_2_4"):
        column["class"] = "col col--span-2"
    for column in soup.select(".et_pb_column_3_4"):
        column["class"] = "col col--span-3"

    delete_classes(
        [
            "et-waypoint",
            "et_pb_image",
            "et_pb_animation_left",
            "et_pb_animation_off",
        ],
        soup,
    )

    # rebuild the page with a simpler stucture
    has_sidebar = soup.find("div", {"id": "sidebar"})
    page_container = soup.new_tag("div")
    page_container["class"] = (
        "page page--sidebar content-container"
        if has_sidebar
        else "page content-container"
    )
    main = soup.new_tag("main")
    page_container.append(main)

    for article in soup.find_all("article"):
        del article["class"]
        main.append(article)

    if has_sidebar:
        print("sidebar!")
        sidebar = soup.select_one("#sidebar").extract()
        sidebar_pkl_path = f"{site_directory}/data/sidebar.pkl"
        if not os.path.exists(sidebar_pkl_path):
            make_directory(f"{site_directory}/data")
            new_sidebar = format_sidebar(soup, sidebar)
            print(new_sidebar.prettify())
            write_pickle(sidebar_pkl_path, new_sidebar)
        page_container.append(get_data_from_file(sidebar_pkl_path))
    if body:
        body.append(page_container)
    else:
        print(soup.prettify())

    return soup


def add_custom_components(
    components_dir: str,
    page_content: BeautifulSoup,
):
    # if custom components have been created for the site, e.g. if you've run the script once and modified the extracted header, footer etc, they can now be added to each page
    if not os.path.exists(components_dir):
        return page_content

    if page_content.find("head"):
        head_html = f"{components_dir}/head.html"
        if os.path.exists(head_html):
            with open(head_html) as new_head:
                soup = BeautifulSoup(new_head, "html.parser")
                soup.find("head").unwrap()
                page_content.find("head").append(soup)

    if page_content.find("body"):
        header_html = f"{components_dir}/header.html"
        if os.path.exists(header_html):
            with open(header_html) as new_header:
                soup = BeautifulSoup(new_header, "html.parser")
                page_content.find("body").insert(0, soup)

        footer_html = f"{components_dir}/footer.html"
        if os.path.exists(footer_html):
            with open(footer_html) as new_footer:
                soup = BeautifulSoup(new_footer, "html.parser")
                page_content.find("body").append(soup)
                footer_html = f"{components_dir}/footer.html"

        script_html = f"{components_dir}/script.html"
        if os.path.exists(script_html):
            with open(script_html) as new_script:
                soup = BeautifulSoup(new_script, "html.parser")
                page_content.find("body").append(soup)

    return page_content


def create_subfolders(parent_directory_path: str, relative_path_to_file: str):
    # once split, the last item is page-name.html, not a directory, so it's removed
    directories = relative_path_to_file.split("/")[:-1]
    for index, dir_name in enumerate(directories):
        relative_path_to_current_dir = "/".join(directories[0 : index + 1])
        path_to_current_dir = f"{parent_directory_path}/{relative_path_to_current_dir}"
        make_directory(path_to_current_dir)


def update_relative_links(relative_path_to_file: str, soup: BeautifulSoup):
    nested_levels = len(relative_path_to_file.split("/")) - 1
    new_prefix = ""
    for level in range(nested_levels):
        new_prefix += "../"
    links = soup.find_all(["a", "img", "link", "script"])

    for link in links:
        attribute = "src" if link.has_attr("src") else "href"
        if link[attribute] and link[attribute].startswith("./"):
            new_path = link[attribute].replace("./", new_prefix)
            link[attribute] = new_path


def write_page_file(directory_path: str, domain: str, url: str, soup: BeautifulSoup):
    if soup is None:
        print(url)
    url_without_trailing_slash = url[:-1] if url[-1] == "/" else url
    url_without_domain = f'{url_without_trailing_slash.replace(f"{domain}", "").replace("http://", "").replace("www.", "")}'
    page_path = (
        url_without_domain[1:]
        if len(url_without_domain) and url_without_domain[0] == "/"
        else url_without_domain
    )
    if page_path == "":
        print('empty page name, renaming to "index"')
        page_path = "index"

    if "/" in page_path:
        create_subfolders(directory_path, page_path)
        update_relative_links(page_path, soup)

    filename = f"{directory_path}/{page_path}.html"
    print(f"{page_path}.html")
    write_file(filename, soup.prettify())


# ---------------------------------------------------------------------------- #


def wordpress_to_static_html(domain: str):
    # set up site directory
    site_directory = create_site_directory(domain)

    # extract common components (head, footer etc) for customization
    # you can manually edit these, put them in './components/' and then run the
    # script again to weave them back into your static pages
    extract_global_elements(domain, site_directory)

    # store the list of pages in a .pkl file so you don't need to re-scrape them
    pages_filepath = f"{site_directory}/data/pages.pkl"
    if not os.path.exists(pages_filepath):
        make_directory(f"{site_directory}/data")
        pages_list = scrape_links(domain)
        write_pickle(pages_filepath, pages_list)
        pprint(pages_list, indent=4)
    pages = get_data_from_file(pages_filepath)
    print("Unique pages:", len(pages))

    # write the html page for each link
    print(f"Writing html files in {site_directory}:")
    for index, url in enumerate(pages):
        cleaned_html = clean_html(site_directory, domain, url)
        if cleaned_html == "rss":
            continue
        rebuilt_page = add_custom_components(
            f"{site_directory}/components", cleaned_html
        )
        write_page_file(site_directory, domain, url, rebuilt_page)
        print(f"{index + 1} / {len(pages)}")
    # url = "http://cameralibre.cc/arts-commons"
    # cleaned_html = clean_html(url)
    # rebuilt_page = add_custom_components(cleaned_html)
    # write_page_file(site_directory, domain, url, rebuilt_page)

    print("DONE!")


if len(sys.argv) > 1:
    input_domain = str(sys.argv[1])
    plain_domain = strip_protocol(input_domain)

    wordpress_to_static_html(plain_domain)
else:
    print(
        "No domain name provided. Please provide your domain name when running the script, eg:\npython3 wordpress-to-static-html.py cameralibre.cc"
    )
